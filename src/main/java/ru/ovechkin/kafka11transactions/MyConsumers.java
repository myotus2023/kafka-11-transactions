package ru.ovechkin.kafka11transactions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MyConsumers {

    @KafkaListener(topics = "topic1", id = "1")
    public void listen1(String message) {
        log.info("TOPIC_1_, получено {} ", message);
    }

    @KafkaListener(topics = "topic1", id = "2")
    public void listen2(String message) {
        log.info("TOPIC__2, получено {} ", message);
    }


}
