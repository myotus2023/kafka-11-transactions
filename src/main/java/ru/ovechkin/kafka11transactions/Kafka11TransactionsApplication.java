package ru.ovechkin.kafka11transactions;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
@SpringBootApplication
public class Kafka11TransactionsApplication {

    public static final String TOPIC_1 = "topic1";
    public static final String TOPIC_2 = "topic2";

    public static void main(String[] args) {
        SpringApplication.run(Kafka11TransactionsApplication.class, args);
    }

    @Bean
    public NewTopic topic1() {
        return TopicBuilder.name(TOPIC_1)
                .build();
    }

    @Bean
    public NewTopic topic2() {
        return TopicBuilder.name(TOPIC_2)
                .build();
    }

    @Bean
    @SneakyThrows
    public ApplicationRunner runner(KafkaTemplate<String, String> template) {
        return args -> {
            try (Producer<String, String> producer = template.getProducerFactory().createProducer()) {
                producer.beginTransaction();

                producer.send(new ProducerRecord<>(TOPIC_1, "Сообщение 1 В подтверждённой транзакции 1"));
                producer.send(new ProducerRecord<>(TOPIC_2, "Сообщение 1 В подтверждённой транзакции 1"));
                Thread.sleep(1000);
                producer.send(new ProducerRecord<>(TOPIC_1, "Сообщение 2 В подтверждённой транзакции 1"));
                producer.send(new ProducerRecord<>(TOPIC_2, "Сообщение 2 В подтверждённой транзакции 1"));
                Thread.sleep(1000);
                producer.send(new ProducerRecord<>(TOPIC_1, "Сообщение 3 В подтверждённой транзакции 1"));
                producer.send(new ProducerRecord<>(TOPIC_2, "Сообщение 3 В подтверждённой транзакции 1"));
                Thread.sleep(1000);
                producer.send(new ProducerRecord<>(TOPIC_1, "Сообщение 4 В подтверждённой транзакции 1"));
                producer.send(new ProducerRecord<>(TOPIC_2, "Сообщение 4 В подтверждённой транзакции 1"));
                Thread.sleep(1000);
                producer.send(new ProducerRecord<>(TOPIC_1, "Сообщение 5 В подтверждённой транзакции 1"));
                producer.send(new ProducerRecord<>(TOPIC_2, "Сообщение 5 В подтверждённой транзакции 1"));

                producer.commitTransaction();


                producer.beginTransaction();

                producer.send(new ProducerRecord<>(TOPIC_1, "Сообщение 1 В откаченной транзакции 2"));
                producer.send(new ProducerRecord<>(TOPIC_2, "Сообщение 1 В откаченной транзакции 2"));
                Thread.sleep(1000);
                producer.send(new ProducerRecord<>(TOPIC_1, "Сообщение 2 В откаченной транзакции 2"));
                producer.send(new ProducerRecord<>(TOPIC_2, "Сообщение 2 В откаченной транзакции 2"));
                Thread.sleep(1000);

                producer.abortTransaction();
            }
        };
    }

}
