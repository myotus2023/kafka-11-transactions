package ru.ovechkin.kafka11transactions;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.kafka.test.context.EmbeddedKafka;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.*;

@SpringBootTest
@EmbeddedKafka(
		partitions = 1,
		brokerProperties = {
				"listeners=PLAINTEXT://localhost:9092",
				"port=9092" ,
				"transaction.state.log.replication.factor=1",
				"transaction.state.log.min.isr=1"
		})
class Kafka11TransactionsApplicationTests {

	@SpyBean
	MyConsumers consumer;

	@Test
	void contextLoads() {
		verify(consumer, times(5)).listen1(contains("В подтверждённой транзакции"));
		verify(consumer, times(5)).listen2(contains("В подтверждённой транзакции"));

		verify(consumer, never()).listen1(contains("В откаченной транзакции"));
		verify(consumer, never()).listen2(contains("В откаченной транзакции"));
	}

}
